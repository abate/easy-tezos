FROM multiarch/ubuntu-core:__BASEIMAGE_ARCH__-bionic

__CROSS_ENV QEMU_EXECVE 1
__CROSS_COPY qemu-__QEMU_ARCH__-static /usr/bin
ENV OPAMYES=true

RUN apt update && apt upgrade -yy && apt install -yy git curl \
    pkg-config libgmp-dev libhidapi-dev libev-dev make m4 unzip aspcud binutils && \
    apt clean && apt --purge -yy autoremove

COPY --from=nomadiclabs/opam:__BASEIMAGE_ARCH__-latest /opam/opam /usr/local/bin/

RUN opam init --bare --disable-sandboxing

ARG BRANCH=mainnet

RUN git clone --depth 1 -b $BRANCH https://gitlab.com/tezos/tezos.git

WORKDIR tezos

RUN make build-deps && \
    eval $(opam config env) && \
    eval $(opam config env) && make && \
    mkdir /_scripts && mkdir /_bin && \
    cp -a scripts/docker/entrypoint.sh /_bin/ && \
    cp -a scripts/docker/entrypoint.inc.sh /_bin/ && \
    cp scripts/alphanet.sh /_scripts/ && \
    cp scripts/alphanet_version /_scripts/ && \
    cp src/bin_client/bash-completion.sh /_scripts/ && \
    cp active_protocol_versions /_scripts/ && \
    cp tezos-* /_bin && \
    strip /_bin/tezos-* && \
    make clean && opam clean
